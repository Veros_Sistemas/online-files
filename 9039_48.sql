/* ********************************************************************** */
/*                                                                        */
/*              SCRIPT DE ATUALIZACAO PARA BANCO 9039                     */
/*              SCRIPT_9039                                            	  */
/*              ALTERAÇÕES PARA SALVAR IMAGENS PRESETUP                   */
/*                                                                        */
/* ********************************************************************** */


/* QUERY 1 ********************************************************************** 

	IMAGEM NO CADASTRO_GERAL

 ********************************************************************************* */
ALTER TABLE CADASTRO_GERAL ADD IMAGEM BLOB sub_type 0;
COMMIT;
 /* END 1   ********************************************************************** */

/* QUERY 2 ********************************************************************** 

	IMAGEM E COMENTARIO NO OPIT_FER

 ********************************************************************************* */
ALTER TABLE OPIT_FER ADD IMAGEM BLOB sub_type 0;
ALTER TABLE OPIT_FER ADD OBS VARCHAR(2000);
COMMIT;
UPDATE OPIT_FER SET OBS='';
COMMIT;
/* END 2   ********************************************************************** */

/* QUERY 3 ********************************************************************** 
 
	CAMPO PARA QTD DE ITENS NOVOS NO ESTOQUE ALMOX

 ********************************************************************************* */
ALTER TABLE ESTOQUE ADD QTD_NOVA DECIMAL(10,4);
COMMIT;
UPDATE ESTOQUE SET QTD_NOVA=0;
COMMIT; 
/* END 3   ********************************************************************** */

/* QUERY 4 ********************************************************************** 
 
	UPDATE PROCEDURE CADASTRA_ESTOQUE

 ********************************************************************************* */
SET TERM ^ ;
ALTER PROCEDURE CADASTRA_ESTOQUE (
    PAR_OBJETO Varchar(12),
    PAR_MESTRE Varchar(12),
    PAR_GRUPO Varchar(12),
    PAR_FAMILIA Varchar(12) )
AS
BEGIN
  
  INSERT INTO ESTOQUE
                (ID_GERADOR_OBJETO,
                ID_MESTRE,
                ID_GRUPO,
                ID_FAMILIA,
                ATIVO,
                QUANTIDADE,
                QTD_ALMOX,
                QTD_NOVA)
        VALUES 
                (:PAR_OBJETO,
                :PAR_MESTRE,
                :PAR_GRUPO,
                :PAR_FAMILIA,
                1,
                0,
                0,
                0);
  
  
   
END^
SET TERM ; ^


GRANT EXECUTE
 ON PROCEDURE CADASTRA_ESTOQUE TO  SYSDBA;
/* END 4   ********************************************************************** */


 /* QUERY CONTROL **************************************************************** */
/*
/*   WRITING NEW VERSION 
/*
/* ********************************************************************** */
UPDATE OR INSERT INTO BANCO (ID_VERSAO,DESCRICAO) VALUES(9039,'IMAGEM PARA PRESETUP QTD_NOVA ESTOQUE ALMOX') MATCHING (ID_VERSAO);
COMMIT;
/* END CONTROL **************************************************************** */

