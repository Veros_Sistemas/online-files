/* ********************************************************************** */
/*                                                                        */
/*              SCRIPT DE ATUALIZACAO PARA BANCO 9038                     */
/*              SCRIPT_9038                                            	  */
/*              TRIGGER PARA PUSH EVENT NA UDP_CONF                       */
/*                                                                        */
/* ********************************************************************** */


/* QUERY 1 ********************************************************************** 

	TRIGGER

 ********************************************************************************* */
SET TERM ^ ;
CREATE TRIGGER UDP_CONF_PUSH FOR UDP_CONFIG
ACTIVE BEFORE UPDATE POSITION 1
AS 
BEGIN 

    IF( (OLD.HDW_ID<>NEW.HDW_ID)OR
        (OLD.IP<>NEW.IP)OR
        (OLD.PORT<>NEW.PORT)OR
        (OLD.TIMEOUT<>NEW.TIMEOUT)OR
        (OLD.BORDA_CONT<>NEW.BORDA_CONT)OR
        (OLD.BORDA_PROD<>NEW.BORDA_PROD)OR
        (OLD.ATIVO<>NEW.ATIVO)OR
        (OLD.USE_BUTTON<>NEW.USE_BUTTON)
    ) 
    THEN POST_EVENT 'UDP_CONF_UPD_CAD';
    ELSE POST_EVENT 'UDP_CONF_UPD_DATA';
END^
SET TERM ; ^/* END 1   ********************************************************************** */

/* QUERY 2 ********************************************************************** 

	INCLUINDO CAMPOS NA UDP_CONFIG

 ********************************************************************************* */
ALTER TABLE UDP_CONFIG ADD LAST_EDGE INTEGER;
ALTER TABLE UDP_CONFIG ADD LAST_ID INTEGER;
ALTER TABLE UDP_CONFIG ADD BLOCK INTEGER;
COMMIT;
UPDATE UDP_CONFIG SET LAST_EDGE=0,LAST_ID=0,BLOCK=0;
COMMIT;
/* END 2   ********************************************************************** */

/* QUERY 3 ********************************************************************** 

	TRIGGER OP_EXEC_CATCHER
	DA UPDATE PARA ESTADO DO CATCHER

 ********************************************************************************* */
SET TERM ^ ;

CREATE TRIGGER OP_EXEC_CATCHER FOR OP_EXEC
ACTIVE AFTER INSERT POSITION 1
AS 
BEGIN 
	UPDATE UDP_CONFIG U SET U.LAST_STATUS='1',U.BLOCK='0',U.LAST_EDGE='1' WHERE U.ID_MAQUINAS=NEW.ID_MAQUINAS; 
END^

SET TERM ; ^ 
/* END 3   ********************************************************************** */

/* QUERY 4 ********************************************************************** 

	ALTERA TRIGGER UDP_CONFIG
	P QDO NOVO REGISTRO, ZERA NOVOS CAMPOS

 ********************************************************************************* */
SET TERM ^ ;
ALTER TRIGGER UDP_CONFIG_BI
ACTIVE BEFORE INSERT POSITION 0
AS
DECLARE VARIABLE tmp DECIMAL(18,0);
BEGIN
  NEW.LAST_EDGE='0';
  NEW.LAST_ID='0';
  NEW.BLOCK='0';
  IF (NEW.ID_UDP_CONFIG IS NULL) THEN
    NEW.ID_UDP_CONFIG = GEN_ID(GEN_ID_UDP_CONFIG, 1);
  ELSE
  BEGIN
    tmp = GEN_ID(GEN_ID_UDP_CONFIG, 0);
    if (tmp < new.ID_UDP_CONFIG) then
      tmp = GEN_ID(GEN_ID_UDP_CONFIG, new.ID_UDP_CONFIG-tmp);
  END
  POST_EVENT 'UDP_CONF_UPD_CAD';
END^
SET TERM ; ^
/* END 4   ********************************************************************** */



/* QUERY 5 ********************************************************************** 

	CRIA TRIGGER PARADA_EXEC NOVA PARA CATCHER

 ********************************************************************************* */
SET TERM ^ ;

CREATE TRIGGER PARADA_EXEC_CATCHER FOR PARADA_EXEC
ACTIVE AFTER INSERT POSITION 1
AS 
BEGIN 
	UPDATE UDP_CONFIG U SET U.LAST_STATUS=3,U.LAST_ID=NEW.ID_MOTIVO_PARADA,U.BLOCK=(SELECT BLOQUEIA FROM CADASTRO_MOTIVO_PARADA WHERE ID_MOTIVO_PARADA=NEW.ID_MOTIVO_PARADA) WHERE U.ID_MAQUINAS=NEW.ID_MAQUINAS;
END^

SET TERM ; ^ 
/* END 5   ********************************************************************** */

/* QUERY 6 ********************************************************************** 

	CRIA TRIGGER DESVIO_EXEC NOVA PARA CATCHER

 ********************************************************************************* */
SET TERM ^ ;

CREATE TRIGGER DESVIO_EXEC_CATCHER FOR DESVIO_EXEC
ACTIVE AFTER INSERT POSITION 1
AS 
BEGIN 
	UPDATE UDP_CONFIG U SET U.LAST_STATUS='2',U.LAST_ID=NEW.ID_MOTIVO_DESVIO,U.BLOCK=(SELECT BLOQUEIA FROM CADASTRO_MOTIVO_DESVIO WHERE ID_MOTIVO_DESVIO=NEW.ID_MOTIVO_DESVIO) WHERE U.ID_MAQUINAS=NEW.ID_MAQUINAS; 
END^

SET TERM ; ^ 
/* END 6   ********************************************************************** */

/* QUERY 7 ********************************************************************** 

	CRIA TRIGGER CADASTRO_MOTIVO_DESVIO NOVA PARA CATCHER

 ********************************************************************************* */
SET TERM ^ ;

CREATE TRIGGER CADASTRO_MOTIVO_DESVIO_CATCHER  FOR CADASTRO_MOTIVO_DESVIO
ACTIVE AFTER UPDATE POSITION 1
AS 
BEGIN 
	UPDATE UDP_CONFIG U SET U.BLOCK=NEW.BLOQUEIA WHERE U.LAST_STATUS='2' AND U.LAST_ID=NEW.ID_MOTIVO_DESVIO;
END^

SET TERM ; ^ 
/* END 7   ********************************************************************** */

/* QUERY 8 ********************************************************************** 

	CRIA TRIGGER CADASTRO_MOTIVO_PARADA NOVA PARA CATCHER

 ********************************************************************************* */
SET TERM ^ ;

CREATE TRIGGER CADASTRO_MOTIVO_PARADA_CATCHER  FOR CADASTRO_MOTIVO_PARADA
ACTIVE AFTER UPDATE POSITION 1
AS 
BEGIN 
	UPDATE UDP_CONFIG U SET U.BLOCK=NEW.BLOQUEIA WHERE U.LAST_STATUS='2' AND U.LAST_ID=NEW.ID_MOTIVO_PARADA;
END^

SET TERM ; ^ 
/* END 8   ********************************************************************** */


/* QUERY 9 ********************************************************************** 

	CRIA TRIGGER DESVIO_EXEC_AU2 PARA CATCHER

 ********************************************************************************* */
SET TERM ^ ;

CREATE TRIGGER DESVIO_EXEC_BU2 FOR DESVIO_EXEC
ACTIVE BEFORE UPDATE POSITION 2
AS 
BEGIN 
	IF ((OLD.DATA_FIM IS NULL)AND(NEW.DATA_FIM IS NOT NULL))
        THEN UPDATE UDP_CONFIG U SET U.LAST_STATUS='1',U.LAST_ID='0',U.LAST_EDGE='0',U.LAST_REPORT=current_timestamp,U.BLOCK='0' WHERE U.ID_MAQUINAS=NEW.ID_MAQUINAS; 
END^

SET TERM ; ^ 
/* END 9   ********************************************************************** */

 /* QUERY CONTROL **************************************************************** */
/*
/*   WRITING NEW VERSION 
/*
/* ********************************************************************** */
UPDATE OR INSERT INTO BANCO (ID_VERSAO,DESCRICAO) VALUES(9038,'TRIGGER PARA PUSH EVENT NA UDP_CONF') MATCHING (ID_VERSAO);
COMMIT;
/* END CONTROL **************************************************************** */

